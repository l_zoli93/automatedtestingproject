package com.example.android.testing.unittesting.TestingProject.validator;

import android.text.Editable;
import android.text.TextWatcher;

public class NameValidator implements TextWatcher {

    private static final String TAG = NameValidator.class.getSimpleName();

    private boolean mIsValidName = false;

    public static boolean isValidName(String name) {
        if (name != null) {
            return name.matches("^[A-Z][a-z\\s]{1,}[\\.]{0,1}[A-Z][a-z\\s]{0,}$");
        }

        return false;
    }

    public boolean isValid() {
        return mIsValidName;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        mIsValidName = isValidName(editable.toString());
    }
}
