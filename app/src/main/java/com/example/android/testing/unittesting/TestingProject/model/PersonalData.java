package com.example.android.testing.unittesting.TestingProject.model;

import java.util.Calendar;

public class PersonalData {

    private final String mName;
    private final Calendar mDateOfBirth;
    private final String mEmail;

    public PersonalData(String name, Calendar dateOfBirth, String email) {
        this.mName = name;
        this.mDateOfBirth = dateOfBirth;
        this.mEmail = email;
    }

    public String getName() {
        return mName;
    }

    public Calendar getDateOfBirth() {
        return mDateOfBirth;
    }

    public String getEmail() {
        return mEmail;
    }
}