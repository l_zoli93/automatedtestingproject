package com.example.android.testing.unittesting.TestingProject.helper;

import android.content.SharedPreferences;

import com.example.android.testing.unittesting.TestingProject.model.PersonalData;

import java.util.Calendar;

public class SharedPreferencesHelper {

    private static final String TAG = SharedPreferencesHelper.class.getSimpleName();

    // Keys for saving values in SharedPreferences.
    public static final String KEY_NAME = "key_name";
    public static final String KEY_DOB = "key_dob_millis";
    public static final String KEY_EMAIL = "key_email";

    // The injected SharedPreferences implementation to use for persistence.
    private final SharedPreferences mSharedPreferences;

    /**
     * Constructor with dependency injection.
     *
     * @param sharedPreferences The {@link SharedPreferences} that will be used in this DAO.
     */
    public SharedPreferencesHelper(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    /**
     * Saves the given {@link PersonalData} that contains the user's settings to
     * {@link SharedPreferences}.
     *
     * @param personalData contains data to save to {@link SharedPreferences}.
     * @return {@code true} if writing to {@link SharedPreferences} succeeded. {@code false}
     * otherwise.
     */
    public boolean savePersonalInfo(PersonalData personalData) {
        // Start a SharedPreferences transaction.
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_NAME, personalData.getName());
        editor.putLong(KEY_DOB, personalData.getDateOfBirth().getTimeInMillis());
        editor.putString(KEY_EMAIL, personalData.getEmail());

        // Commit changes to SharedPreferences.
        return editor.commit();
    }

    /**
     * Retrieves the {@link PersonalData} containing the user's personal information from
     * {@link SharedPreferences}.
     *
     * @return the Retrieved {@link PersonalData}.
     */
    public PersonalData getPersonalInfo() {
        // Get data from the SharedPreferences.
        String name = mSharedPreferences.getString(KEY_NAME, "");
        Long dobMillis =
                mSharedPreferences.getLong(KEY_DOB, Calendar.getInstance().getTimeInMillis());
        Calendar dateOfBirth = Calendar.getInstance();
        dateOfBirth.setTimeInMillis(dobMillis);
        String email = mSharedPreferences.getString(KEY_EMAIL, "");

        // Create and fill a PersonalData model object.
        return new PersonalData(name, dateOfBirth, email);
    }
}