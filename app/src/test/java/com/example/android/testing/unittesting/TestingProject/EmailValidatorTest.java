package com.example.android.testing.unittesting.TestingProject;

import com.example.android.testing.unittesting.TestingProject.validator.EmailValidator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EmailValidatorTest {

    @Test
    public void emailValidator_CorrectEmailSimple_ReturnsTrue() {
        assertTrue(EmailValidator.isValidEmail("name@email.com"));
    }

    @Test
    public void emailValidator_CorrectEmailSubDomain_ReturnsTrue() {
        assertTrue(EmailValidator.isValidEmail("name@email.co.uk"));
    }

    @Test
    public void emailValidator_InvalidEmailNoTld_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail("name@email"));
    }

    @Test
    public void emailValidator_InvalidEmailDoubleDot_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail("name@email..com"));
    }

    @Test
    public void emailValidator_InvalidEmailNoUsername_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail("@email.com"));
    }

    @Test
    public void emailValidator_EmptyString_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail(""));
    }

    @Test
    public void emailValidator_NullEmail_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail(null));
    }

    @Test
    public void emailValidator_OnlyNumberEmail_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail("1234"));
    }

    @Test
    public void emailValidator_OnlySpecialCharacterEmail_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail("*%"));
    }

    @Test
    public void emailValidator_SpaceEmail_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail(" "));
    }

    @Test
    public void emailValidator_MissingAtFromEmail_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail("nameemail.com"));
    }
}