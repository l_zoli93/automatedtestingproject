package com.example.android.testing.unittesting.TestingProject;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.android.testing.unittesting.TestingProject.helper.SharedPreferencesHelper;
import com.example.android.testing.unittesting.TestingProject.model.PersonalData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = "src/main/AndroidManifest.xml")
public class MainActivityTest {

    private MainActivity mainActivity;

    @Before
    public void setup() {
        mainActivity = Robolectric.setupActivity(MainActivity.class);
    }

    @Test
    public void clickingSaveButton() {
        Button button = (Button) mainActivity.findViewById(R.id.saveButton);
        button.performClick();

        assertThat(button.getText().toString(), equalTo("Save"));
    }

    @Test
    public void clickingRevertButton() {
        Button button = (Button) mainActivity.findViewById(R.id.revertButton);
        button.performClick();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mainActivity);
        SharedPreferencesHelper mSharedPreferencesHelper = new SharedPreferencesHelper(sharedPreferences);
        PersonalData data = mSharedPreferencesHelper.getPersonalInfo();
        EditText nameEditText = (EditText) mainActivity.findViewById(R.id.userNameInput);
        EditText emailEditText = (EditText) mainActivity.findViewById(R.id.emailInput);

        assertThat(nameEditText.getText().toString(), equalTo(data.getName()));
        assertThat(emailEditText.getText().toString(), equalTo(data.getName()));
    }
}