package com.example.android.testing.unittesting.TestingProject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import android.content.SharedPreferences;

import com.example.android.testing.unittesting.TestingProject.helper.SharedPreferencesHelper;
import com.example.android.testing.unittesting.TestingProject.model.PersonalData;

import java.util.Calendar;

@RunWith(MockitoJUnitRunner.class)
public class SharedPreferencesHelperTest {

    private static final String TEST_NAME = "Test name";
    private static final String TEST_EMAIL = "test@email.com";
    private static final Calendar TEST_DATE_OF_BIRTH = Calendar.getInstance();

    static {
        TEST_DATE_OF_BIRTH.set(1993, 1, 2);
    }

    private PersonalData mPersonalData;
    private SharedPreferencesHelper mMockSharedPreferencesHelper;
    private SharedPreferencesHelper mMockBrokenSharedPreferencesHelper;

    @Mock
    SharedPreferences mMockSharedPreferences;

    @Mock
    SharedPreferences mMockBrokenSharedPreferences;

    @Mock
    SharedPreferences.Editor mMockEditor;

    @Mock
    SharedPreferences.Editor mMockBrokenEditor;

    @Before
    public void initMocks() {
        mPersonalData = new PersonalData(TEST_NAME, TEST_DATE_OF_BIRTH, TEST_EMAIL);
        mMockSharedPreferencesHelper = createMockSharedPreference();
        mMockBrokenSharedPreferencesHelper = createBrokenMockSharedPreference();
    }

    @Test
    public void sharedPreferencesHelper_SaveAndReadPersonalInformation() {
        // Save the personal information to SharedPreferences
        boolean success = mMockSharedPreferencesHelper.savePersonalInfo(mPersonalData);

        assertThat("Checking that PersonalData.save... returns true", success, is(true));

        // Read personal information from SharedPreferences
        PersonalData savedPersonalData = mMockSharedPreferencesHelper.getPersonalInfo();

        // Make sure both written and retrieved personal information are equal.
        assertThat("Checking that PersonalData.name has been persisted and read correctly",
                mPersonalData.getName(),
                is(equalTo(savedPersonalData.getName())));

        assertThat("Checking that PersonalData.dateOfBirth has been persisted and read "
                        + "correctly",
                mPersonalData.getDateOfBirth(),
                is(equalTo(savedPersonalData.getDateOfBirth())));

        assertThat("Checking that PersonalData.email has been persisted and read "
                        + "correctly",
                mPersonalData.getEmail(),
                is(equalTo(savedPersonalData.getEmail())));

        verifyNoMoreInteractions(mMockBrokenSharedPreferences);
    }

    @Test
    public void sharedPreferencesHelper_SavePersonalInformationFailed_ReturnsFalse() {
        // Read personal information from a broken SharedPreferencesHelper
        boolean success = mMockBrokenSharedPreferencesHelper.savePersonalInfo(mPersonalData);

        assertThat("Makes sure writing to a broken SharedPreferencesHelper returns false", success, is(false));

        verifyZeroInteractions(mMockSharedPreferences);
    }

    /**
     * Creates a mocked SharedPreferences.
     */
    private SharedPreferencesHelper createMockSharedPreference() {
        // Mocking reading the SharedPreferences as if mMockSharedPreferences was previously written
        // correctly.
        when(mMockSharedPreferences.getString(eq(SharedPreferencesHelper.KEY_NAME), anyString()))
                .thenReturn(mPersonalData.getName());
        when(mMockSharedPreferences.getString(eq(SharedPreferencesHelper.KEY_EMAIL), anyString()))
                .thenReturn(mPersonalData.getEmail());
        when(mMockSharedPreferences.getLong(eq(SharedPreferencesHelper.KEY_DOB), anyLong()))
                .thenReturn(mPersonalData.getDateOfBirth().getTimeInMillis());

        // Mocking a successful commit.
        when(mMockEditor.commit()).thenReturn(true);

        // Return the MockEditor when requesting it.
        when(mMockSharedPreferences.edit()).thenReturn(mMockEditor);
        return new SharedPreferencesHelper(mMockSharedPreferences);
    }

    /**
     * Creates a mocked SharedPreferences that fails when writing.
     */
    private SharedPreferencesHelper createBrokenMockSharedPreference() {
        // Mocking a commit that fails.
        when(mMockBrokenEditor.commit()).thenReturn(false);

        // Return the broken MockEditor when requesting it.
        when(mMockBrokenSharedPreferences.edit()).thenReturn(mMockBrokenEditor);
        return new SharedPreferencesHelper(mMockBrokenSharedPreferences);
    }
}