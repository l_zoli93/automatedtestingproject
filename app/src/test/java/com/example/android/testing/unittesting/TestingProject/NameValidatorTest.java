package com.example.android.testing.unittesting.TestingProject;

import com.example.android.testing.unittesting.TestingProject.validator.NameValidator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class NameValidatorTest {

    @Test
    public void nameValidator_CorrectFullName_ReturnsTrue() {
        assertTrue(NameValidator.isValidName("User Name"));
    }

    @Test
    public void nameValidator_InvalidShortName_ReturnsTrue() {
        assertFalse(NameValidator.isValidName("Name"));
    }

    @Test
    public void nameValidator_InvalidNameContainsNumber1_ReturnsFalse() {
        assertFalse(NameValidator.isValidName("User1 Name"));
    }

    @Test
    public void nameValidator_InvalidNameContainsNumber2_ReturnsFalse() {
        assertFalse(NameValidator.isValidName("User Name2"));
    }

    @Test
    public void nameValidator_InvalidNameContainsSpecialCharacter_ReturnsFalse() {
        assertFalse(NameValidator.isValidName("User% Name*"));
    }

    @Test
    public void nameValidator_EmptyString_ReturnsFalse() {
        assertFalse(NameValidator.isValidName(""));
    }

    @Test
    public void nameValidator_NullName_ReturnsFalse() {
        assertFalse(NameValidator.isValidName(null));
    }

    @Test
    public void nameValidator_OnlyNumberName_ReturnsFalse() {
        assertFalse(NameValidator.isValidName("1234"));
    }

    @Test
    public void nameValidator_OnlySpecialCharacter_ReturnsFalse() {
        assertFalse(NameValidator.isValidName("*"));
    }
}